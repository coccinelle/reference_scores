# reference_scores

Reference scores for Coccinelle regression tests running by CI. CI will commit new scores for every version pushed on coccinelle/coccinelle.

NOTE: This repository is no longer used by the Coccinelle CI. As of 2025-02-19, the Coccinelle CI expects 100% on all tests not explicitly marked as failures. This repository is still available as a reference, but is no longer useful or maintained.